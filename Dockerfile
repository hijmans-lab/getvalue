#docker run -d -p 8080:80 extract
FROM flask

FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install -y python-pip
RUN apt-get install -y nginx supervisor
RUN apt-get install -y build-essential python-numpy python-gdal libgdal-dev python-dev
RUN pip install uwsgi Flask rasterio

VOLUME ["/home/vulpes","/var/log/docker":"/var/log"]


ADD ./app /app
ADD ./config /config

RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default

RUN ln -s /config/nginx.conf /etc/nginx/sites-enabled/
RUN ln -s /config/supervisor.conf /etc/supervisor/conf.d/

EXPOSE 80

CMD ["supervisord", "-n"]
