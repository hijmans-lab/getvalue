# Project CodeName:getValue

This project creates a server/service that allows for point querying of raster data sets via a url.
It's kinda like WMS GetFeatureInfo except much simpler and uses a REST style API.

Author: Alex Mandel
Copyright: University of California, Davis
License: TBD Open Source

## Deploy

See the [Readme](https://bitbucket.org/aimandel/getValue/src/master/app/) inside the app folder.

## Clients

There's a [web client](test2.biogeo.ucdavis.edu/getValue)
We do plan to create and R package, and possibly a QGIS plugin for easier usage.

## Docker-Flask Setup - Not working
Originally was going to try making it run in docker, but that got complicated enough to not be worthwhile for the prototype. See above for how to deploy.

Docker-Flask is a Docker setup for a very simple Flask application. It runs on Python 2.x via Nginx and UWSGI. 
The apps folder contains example applications for testing.

### Step 1: Install Docker

### Step 2: Download docker-flask
```bash
$  git clone 
$  cd docker-flask
```

### Step 3: Build docker-flask
```bash
$  docker build --rm --tag=flask .
```

## Step 4: Run docker-flask

This runs it interactively
```bash
$  docker run -it -p 8080:80 flask
```

You should see Docker running a supervisord session with Nginx and UWSGI. Now all that's left to do is open up your browser and type the IP address ```localhost:8080```

You should see the text *Flask is running!* in your browser. That's all there is to it!
