#Currently require python 2.7 because of gdal
#TODO: upgrade to python 3 when gdal 2.0 ships as package
# http://localhost:5000/extract/worldpop2010?x=3.395833&y=6.453056
from flask import Flask, request, json, jsonify, make_response, render_template
from extract import findLayer, listLayers, getValue


app = Flask(__name__)

@app.route('/')
def index():
    #return 'See a list of layers @ /layers'
    ''' Return a list of layers'''
    layers = listLayers('layers.catalog')
    return render_template('index.html', layers=layers)

@app.route('/layers')
def layers():
    ''' Return a list of layers'''
    layers = listLayers('layers.catalog')
    response = make_response(json.dumps(layers))
    response.headers['content-type'] = 'application/json'
    return response

@app.route('/layer/<layer>')
def extract(layer):
    ''' Given a layer, Lat(y) and Lon(x), Return the raster value'''
    x = request.args.get('x', '')
    y = request.args.get('y', '')
    #Get layer list from catalog file, find match, pass path to getValue
    #lyr = ''.join(["/home/vulpes/biogeo/gfcp/gfc-other/test/", layer, ".tif"])
    #TODO: how to make sure path to catalog can be found?
    #TODO: app.open_resource()?
    lyr = findLayer('layers.catalog', layer)
    
    #Extract the value
    val = getValue(lyr, x, y)
    #return 'Going to extract Values from %s, %s' % (layer,val)
    data = {"type": "Feature",
            "geometry":{"type": "Point", 
                "coordinates":[float(x),float(y)]
                },
            "properties":{
                "layer":layer,
                "value":val
                }
            }
    gjson = json.dumps(data)
    response = make_response(gjson)
    response.headers['content-type'] = 'application/json'
    #return a geojson response
    return response
    


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
