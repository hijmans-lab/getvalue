var map,
    point,
    userloc = {"lat": 0, "lon": 0, "acc": 0};

function onEachFeature(feature, layer) {
    "use strict";
    // Render the properties of the point
    if (feature.properties) {
        var description = feature.properties.layer + ":" + feature.properties.value.toString();
        layer.bindPopup(description);
    }
}


function getValue(e) {
    "use strict";
    //Take selected layer and map click x,y to find value
    var layer,
        gurl,
        $layerselect = $("#layerSelect");
    //var layer = "worldpop"; //Get this from selection on page
    layer = $layerselect.val();
    //TODO: clean this up into a parameter and url formatting
    gurl = 'layer/' + layer + "?x=" + e.latlng.lng + "&y=" + e.latlng.lat;
    //console.info(gurl);
    $.getJSON(gurl, function (json) {
        var geojsonFeature;
        geojsonFeature = json;
        //growers.addData(geojsonFeature);
        point = L.geoJson(geojsonFeature, {
            onEachFeature: onEachFeature
        }).addTo(map);
    });
    
}


function initMap() {
    "use strict";
    map = L.map('map').setView([userloc.lat, userloc.lon], 2);
    
//    L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/{type}/{z}/{x}/{y}.{ext}', {
//	type: 'sat',
//	ext: 'jpg',
//	attribution: 'Tiles Courtesy of <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Portions Courtesy NASA/JPL-Caltech and U.S. Depart. of Agriculture, Farm Service Agency',
//	subdomains: '1234'
//}).addTo(map);
    
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    
    map.on('click', getValue);

    //Outline test region
    var bounds = [[6, 3], [7, 4]];
    L.rectangle(bounds, {stroke: true, 
                         color: "#E81919", 
                         weight: 5,
                         fillOpacity: 0.1,
                         opacity: 0.8,
                        }).addTo(map);
    map.setView([6.5, 3.5], 8);
}


$(document).ready(function () {
    "use strict";
    // Get the location from the web browser if user approves
    //navigator.geolocation.getCurrentPosition(GetLocation, NoLocation, locoptions);
    
    // Initialize the map
    //map = L.map('map').setView([38.5725, -121.4680], 13);
    initMap();
    //growers = L.geoJson().addTo(map);
    $.getJSON('layers', function (json) {
        var $layerslist = $("#layers"),
            $layerselect = $("#layerSelect");
        $.each(json, function (index, value) {
            var text = "<li>" + value +  "</li>",
                selection = '<option value="'+ value + '">' + value + '</option>';
            $layerslist.append(text);
            $layerselect.append(selection);
        });
    });

});