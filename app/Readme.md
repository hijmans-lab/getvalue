# Extract Value at X Y


# Install Deps

How to install on Ubuntu 14.04
Ubuntugis-unstable? Uwsgi or Gunicorn?

## system gdal-bin, uwsgi
```sh 
sudo apt-add-repository -y ppa:ubuntugis/ubuntugis-unstable
sudo apt-get update
sudo apt-get install -y build-essential python-pip \
                        #python-numpy \
                        #python-gdal \
                        libgdal-dev \
                        python-dev \
                        gdal-bin \
                        supervisor \
                        #gunicorn \
                        #uwsgi uwsgi-plugin-python
```

# Install virtualenv
```sh
sudo pip install virtualenv
```

# Create virtualenv
```sh

virtualenv py27getValue
```

# activate
```sh
cd py27getValue
source bin/activate
```
## clone repo
```sh
git clone git@bitbucket.org:aimandel/extract.git
```


# Install required software

## pip flask, rasterio, python-gdal
```sh
pip install numpy
pip install -r getValue/requirements.txt
# OR 
pip install numpy
# Version must match system gdal version
pip install pygdal==1.11.2.1
pip install flask rasterio
```
# deactivate
# config uwsgi or gunicorn
# config apache/nginx proxy

## References
* https://github.com/perrygeo/python-rasterstats/tree/master/src/rasterstats
* https://github.com/mapbox/rasterio
* https://community.esri.com/docs/DOC-2954
* https://docs.scipy.org/doc/numpy-dev/user/quickstart.html
* http://geoexamples.blogspot.com/2012/12/raster-calculations-with-gdal-and-numpy.html


# Deployment methods

## Gunicorn

Verify that it works with Gunicorn
```
source bin/activate
pip install gunicorn
cd getValue/getValue/app
gunicorn --workers=2 -b 0.0.0.0:8000 app:app
```

## Supervisor w/ Gunicorn
```
sudo apt-get install supervisor
source bin/activate
pip install gunicorn
sudo cp config/getValue.conf /etc/supervisor/conf.d/
sudo service supervisor start
```

# Preparing Data

## Build a vrt
```sh
gdalbuildvrt srtm30mv3.vrt ~/hadoop/library/elevation/srtm/v3tif/*.tif
#Note this makes absolute paths to the files.
# TODO sed replace /home/user with ~/
sed -i 's|/home/vulpes/hadoop/library/elevation/srtm|/share/ssd/data|' srtm30mv3ssd.vrt 
```
