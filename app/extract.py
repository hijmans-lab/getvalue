import rasterio
import numpy
import math
import resource
import ConfigParser #configparser in py3

#TODO create a class to store the catalog to avoid reading all the time?
def findLayer(catalog, lyr):
    '''
    Lookup the local path to the layer based on the name.
    Return the path.
    '''
    config = ConfigParser.SafeConfigParser()
    config.read(catalog)
    #TODO trap 
    lyrPath = config.get('layers',lyr)
    return(lyrPath)
    
def listLayers(catalog):
    ''' 
    Input a path to a catalog of layers
    Return a list of valid layers on this server
    '''
    config = ConfigParser.SafeConfigParser()
    config.read(catalog)
    layers = config.options('layers')
    return(layers)

def getValue(rast,x,y):
    with rasterio.drivers():
        with rasterio.open(rast) as src:
            #Better way to get row and col from sgillies
            #http://gis.stackexchange.com/questions/109273/error-reading-large-virtual-raster-with-rasterio-cannot-invert-degenerate-tran#109382
            row, col = src.index(float(x), float(y))
            #Reading windows takes 5-16ms vs reading whole set takes 4s with almost no ram usage
            result = float(src.read(1, window=((row,row+1),(col,col+1))))
    #Next line attempts to measure memory is use
    #resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1000
    return(result)

if __name__ == "__main__":
    x = 3.395833
    y = 6.453056
    data = 'srtm'
    #data = 'tmean12'
    rast = findLayer('/home/vulpes/code/pythonenv/py27getValue/getValue/app/layers.catalog',data)
    result = getValue(rast, x, y)   
    print(result)